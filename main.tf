terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.11.0"
    }
  }
}

provider "gitlab" {
  # Configuration optionsglpat-51YbhH8ZbT3wCyd5W7D9
  token = var.gitlab_token
  base_url = "https://gitlab.com/"

}

resource "gitlab_project" "terraform_example" {
  name = "demo_terraform1"
  visibility_level = "public"
}